//
//  ViewController.swift
//  ARDice
//
//  Created by Radu Baloi on 15/01/2020.
//  Copyright © 2020 teisoftware. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {
    //init empty arr of SCNN
    var diceArray = [SCNNode]()
    
    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        //self.sceneView.debugOptions = [ARSCNDebugOptions.showFeaturePoints]
        
        sceneView.autoenablesDefaultLighting = true
          
        
//        //SUN
//        let sphere = SCNSphere(radius: 0.2)
//        //create material
//        let material = SCNMaterial()
//        material.diffuse.contents = UIImage(named: "art.scnassets/8k_sun.jpg") //UIColor.red
//        //add material/s to the cube
//        sphere.materials = [material]
//        //point in 3d space + object to display
//        let node = SCNNode()
//        node.position = SCNVector3(x: 0, y: 0.1, z: -0.45) //negative z -> towards camera
//        //assign object to node
//        node.geometry = sphere
//        sceneView.scene.rootNode.addChildNode(node)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if ARWorldTrackingConfiguration.isSupported {
            // Create a session configuration // -WorldTracking -> fake ar
            let configuration = ARWorldTrackingConfiguration()
            
            //ENUM .x  .y  .z
            configuration.planeDetection = .horizontal
            
            // Run the view's session
            sceneView.session.run(configuration)
        } else {
            let alert = UIAlertController(title: "Your device does not support ARWorldTracking", message: "test", preferredStyle: .alert)

            alert.addAction(UIAlertAction(title: "ok", style: .default, handler: nil))

            self.present(alert, animated: true)
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    //MARK: - Dice rendering methods
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let touchLocation = touch.location(in: sceneView)
            
            let results = sceneView.hitTest(touchLocation, types: .existingPlaneUsingExtent) //results array
            if let hitResult = results.first { //localTransform, worldTransform(used to place the dice:)
                addDice(atLocation: hitResult)
            }
        }
    }
    
    func addDice(atLocation location: ARHitTestResult) {
        //Create a new scene - dice node -set position = 3d real position from screen touch 2d - add to root
        let diceScene = SCNScene(named: "art.scnassets/diceCollada.scn")!
        
        //hitResults - 4x4 matrix 0.. scale,rotation..,   3->position
        if let diceNode = diceScene.rootNode.childNode(withName: "Dice", recursively: true) {//rec ->searc in subtrees
            diceNode.position = SCNVector3(
            x: location.worldTransform.columns.3.x,
            y: location.worldTransform.columns.3.y + diceNode.boundingSphere.radius , //elevate half dice width from plane
            z: location.worldTransform.columns.3.z
            )
            

            diceArray.append(diceNode)
            
            
            sceneView.scene.rootNode.addChildNode(diceNode)
            
            roll(dice: diceNode)
            
        }
    }
    
    func rollAll() {
          if !diceArray.isEmpty {
              for dice in diceArray {
                  roll(dice: dice)
              }
          }
      }
      
      func roll(dice: SCNNode) {
          //Rotate in 2 axis (x,z)  -rotating y axis does not change the face on top
              //all 4 faces equal chances to be on top (rand num 1-4) * (90degree)
              let randomX = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
              let randomZ = Float(arc4random_uniform(4) + 1) * (Float.pi/2)
              dice.runAction( //diceNode.
                  SCNAction.rotateBy(x: CGFloat(randomX * 7), //speed up rotations
                                     y: 0,
                                     z: CGFloat(randomZ + 7),
                                     duration: 0.5))
        
    }
    
    @IBAction func rollAgain(_ sender: UIBarButtonItem) {
        rollAll()
    }
    
    //Remove each dice from parent node
    @IBAction func removeAllDice(_ sender: UIBarButtonItem) {
        if !diceArray.isEmpty {
            for dice in diceArray {
                dice.removeFromParentNode()
            }
        }
    }
    
    override func motionEnded(_ motion: UIEvent.EventSubtype, with event: UIEvent?) {
        //shake to roll
        rollAll()
    }
    
    //MARK: - ARSCNViewDelegateMethods
    //detect horizontal surface,give it a width and a height , blank node: SCNNode
    func renderer(_ renderer: SCNSceneRenderer, didAdd node: SCNNode, for anchor: ARAnchor) {
//        if anchor is ARPlaneAnchor { //if it is ARPlaneAnchor, change its type

//            let planeAnchor = anchor as! ARPlaneAnchor
//
//        } else {
//            return
//        }
        
        //            //specify width and height of the plane detected
        //try to downcast else optional = nil ->fails->return (continue)
        guard let planeAnchor = anchor as? ARPlaneAnchor else {return}
        
        let planeNode = createPlane(withPlaneAnchor: planeAnchor)
        // sceneView.scene.rootNode.addChildNode or use the blank node:
        node.addChildNode(planeNode)
    }
    
}


func createPlane(withPlaneAnchor planeAnchor: ARPlaneAnchor) -> SCNNode {
    //convert dimensions of anchor to a scenePlane; A PlaneAnchor is 3DVector but is always 2D
    let plane = SCNPlane(width: CGFloat(planeAnchor.extent.x), height: CGFloat(planeAnchor.extent.z))

    //Create a plane node
    let planeNode = SCNNode()
    //flat horizontal plane -> height0
    planeNode.position = SCNVector3(x: planeAnchor.center.x,y: 0 ,z: planeAnchor.center.z)

    //the plane created is vertical, rotate half PI / 90' clockwise, around axis: x
    planeNode.transform = SCNMatrix4MakeRotation(-Float.pi/2, 1, 0, 0)

    let gridMaterial = SCNMaterial()
    gridMaterial.diffuse.contents = UIImage(named: "art.scnassets/grid.png")
    plane.materials = [gridMaterial]

    planeNode.geometry = plane
    return planeNode
}
